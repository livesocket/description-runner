FROM golang:alpine as base
RUN apk --no-cache add git ca-certificates g++
WORKDIR /repos/description-runner
ADD go.mod go.sum ./
RUN go mod download

FROM base as builder
WORKDIR /repos/description-runner
ADD . .
RUN CGO_ENABLED=0 GOOS=linux go build -o description-runner

FROM scratch as release
COPY --from=builder /repos/description-runner/description-runner /description-runner
EXPOSE 8080
ENTRYPOINT ["/description-runner"]