module gitlab.com/livesocket/description-runner

go 1.12

require (
	github.com/gammazero/nexus/v3 v3.0.0
	github.com/gempir/go-twitch-irc/v2 v2.2.1
	github.com/mattn/go-sqlite3 v1.13.0 // indirect
	gitlab.com/livesocket/conv v0.0.0-20191012101209-727e1c03a95c
	gitlab.com/livesocket/service/v2 v2.0.11
	golang.org/x/crypto v0.0.0-20191128160524-b544559bb6d1 // indirect
)
